package net.toften.casino;

public interface Game {
	public int getMinPlayers();
	
	public int getMaxPlayers();

	public double getPot();
}
