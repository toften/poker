package net.toften.casino;

public interface Croupier {
	public boolean raise(double amount);
	
	public boolean check();
	
	public boolean call();

	public BetTypes[] getAllowedBetTypes();
}
