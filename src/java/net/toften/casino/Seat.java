package net.toften.casino;

public interface Seat {
	public boolean leave();
	
	public boolean stand();
}
