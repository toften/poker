package net.toften.casino.card;

import java.util.Arrays;
import java.util.List;

import net.toften.casino.card.poker.texasholdem.Round;

public class Hand {
	protected Round t;
	private int minCardCount, maxCardCount, currentCardCount;
	private Card[] cards;
		
	public Hand(Round t, Card[] cards, int minCardCount, int maxCardCount) {
		this.t = t;
		this.minCardCount = minCardCount;
		this.maxCardCount = maxCardCount;
		
		cards = new Card[maxCardCount];
		
		if (cards == null) {
			if (minCardCount > 0)
				throw new IllegalArgumentException("Wrong number of cards in hand");				

			} else {
			if (cards.length < minCardCount)
				throw new IllegalArgumentException("Wrong number of cards in hand");
			
			while(currentCardCount < cards.length) {
				this.cards[currentCardCount] = cards[currentCardCount];
				currentCardCount++;
			}
		}
	}
	
	public Card[] getCards() {
		return cards;
	}
	
	public void addCard(Card c) {
		if (cards.length == maxCardCount)
			throw new IllegalArgumentException("No more cards can be added");

		cards[currentCardCount++] = c;
	}
	
	public Card removeCard(int index) {
		Card c = cards[index];
		
		List<Card> cl = Arrays.asList(c);
		cl.remove(index);
		cl.toArray(cards);
		
		return c;
	}

}
