package net.toften.casino.card;

import net.toften.casino.Table;

public interface CardTable extends Table {
	public int getDeckCount();
}
