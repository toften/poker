package net.toften.casino.card;

import net.toften.casino.Game;

public interface CardGame extends Game {
	public Card[] getHand();

	public int getDealerIndex();
}
