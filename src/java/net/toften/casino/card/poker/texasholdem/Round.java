package net.toften.casino.card.poker.texasholdem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.toften.casino.card.Card;
import net.toften.casino.card.Deck;
import net.toften.casino.card.Hand;
import net.toften.casino.card.TurnListener;

public class Round {
	private List<TurnListener> turnListeners = new ArrayList<TurnListener>();
	
	private Deck deck;
	
	private Hand[] hands;
	private Hand winner;
	
	private List<Card> tableCards = new ArrayList<Card>();

	private RoundState roundState;
	
	private int players;
	
	private void turnCard() {
		Card c = deck.getCards(1)[0];
		
		tableCards.add(c);
		
		updateTurnListener(c);
	}

	public Round(int players) {
		this.players = players;
		
		deck = new Deck();
		
		hands = new Hand[players];
	}
	
	public Card[] getTableCards() {
		Card[] a = new Card[tableCards.size()];
		
		tableCards.toArray(a);
		
		return a;
	}

	public Hand[] getHands() {
		return hands;
	}

	public RoundState getTableState() {
		return roundState;
	}


	/*
	 * State change methods
	 */
	public Hand[] deal() {
		if (roundState != RoundState.START)
			throw new IllegalStateException("Must be in START state to deal");
		
		for (int i = 0; i < players; i++) {
			hands[i] = new Hand(this, deck.getCards(2), 2, 2);
		}
		
		roundState = RoundState.DEAL;
		
		return hands;
	}

	public void burn() {
		if (roundState != RoundState.DEAL)
			throw new IllegalStateException("Must be in DEAL state to burn");
		
		deck.burnCards(1);
		
		roundState = RoundState.BURN;
		
	}

	public void flop() {
		if (roundState != RoundState.BURN)
			throw new IllegalStateException("Must be in BURN state to flop");
		
		turnCard();
		turnCard();
		turnCard();
		
		roundState = RoundState.FLOP;
		
	}

	public void turn() {
		if (roundState != RoundState.FLOP)
			throw new IllegalStateException("Must be in FLOP state to turn");
		
		turnCard();
		
		roundState = RoundState.TURN;
		
	}

	public void river() {
		if (roundState != RoundState.TURN)
			throw new IllegalStateException("Must be in TURN state to river");
		
		turnCard();
		
		roundState = RoundState.RIVER;
		
	}

	public Hand showdown() {
		if (roundState != RoundState.RIVER)
			throw new IllegalStateException("Must be in RIVER state to showdown");
		
		Arrays.sort(hands);
		
		winner = hands[hands.length - 1];
		
		roundState = RoundState.SHOWDOWN;
		
		return winner;
		
	}

	/*
	 * Turn listener methods
	 */
	public void addTurnListener(TurnListener l) {
		turnListeners.add(l);
	}

	public void removeTurnListener(TurnListener l) {
		turnListeners.remove(l);
	}
	
	public void updateTurnListener(Card c) {
		for (TurnListener t : turnListeners)
			t.cardDealt(c);
		
	}
	
}
