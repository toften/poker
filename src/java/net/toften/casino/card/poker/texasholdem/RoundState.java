package net.toften.casino.card.poker.texasholdem;


/**
 * The RoundState indicates the current state of the
 * table.
 * 
 * For example if the RoundState is DEAL, that means the
 * the cards <i>has</i> been dealt to the hands.
 * 
 * @author thomas
 *
 */
public enum RoundState {
	START,
	DEAL,
	BURN,
	FLOP,
	TURN,
	RIVER,
	SHOWDOWN;
	
	public static void nextState(Round t) {
		switch (t.getTableState()) {
		case START:
			t.deal();
			break;
			
		case DEAL:
			t.burn();
			break;
			
		case BURN:
			t.flop();
			break;
			
		case FLOP:
			t.turn();
			break;
			
		case TURN:
			t.river();
			break;
			
		case RIVER:
			t.showdown();
			break;
		}
	}
}
