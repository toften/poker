package net.toften.casino.card.poker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.toften.casino.card.Card;
import net.toften.casino.card.Hand;
import net.toften.casino.card.Card.Rank;
import net.toften.casino.card.Card.Suit;
import net.toften.casino.card.poker.texasholdem.Round;

public enum PokerHandTypes {
	NOHAND,
	NOPAIR,
	ONEPAIR,
	TWOPAIR,
	THREE,
	STRAIGHT,
	FLUSH,
	FULLHOUSE,
	FOUR,
	STRAIGHTFLUSH,
	ROYALFLUSH;
	
	public static PokerHandTypes check(Hand hand, Round round) {
		return check(convert(hand, round));
	}
		
		
	public static PokerHandTypes check(Card[] deck) {
		if (deck.length < 5)
			return NOHAND;
		
		Arrays.sort(deck);
		
		Rank firstRank = isStraight(deck);
		if (firstRank != null) {
			if (isFlush(deck) != null) {
				if (firstRank == Rank.TEN)
					return ROYALFLUSH;
				else
					return STRAIGHTFLUSH;
			}
			else
				return STRAIGHT;
		}
		
		if (isFlush(deck) != null)
			return FLUSH;
		
		if (hasFour(deck) != null)
			return FOUR;
		
		if (hasFullHouse(deck))
			return FULLHOUSE;
		
		if (hasThree(deck) != null)
			return THREE;
		
		if (countPairs(deck) >= 2)
			return TWOPAIR;
		
		if (countPairs(deck) == 1)
			return ONEPAIR;
			
		return NOPAIR;
	}
	
	public static Card[] convert(Hand hand, Round round) {
		List<Card> a = new ArrayList<Card>();
		
		for(Card o : hand.getCards())
			a.add(o);
		
		for(Card o : round.getTableCards())
			a.add(o);
		
		Card[] returnValue = new Card[hand.getCards().length + round.getTableCards().length];
		a.toArray(returnValue);
		Arrays.sort(returnValue);
		
		return returnValue;
		
	}
	
	private static int countPairs(Card[] deck) {
		int pairs = 0;
		Rank lastRank = null;
		
		for(Card c : deck) {
			if (c.getRank() == lastRank) {
				pairs++;
				lastRank = null;
			} else
				lastRank = c.getRank();
		}
		
		return pairs;
	}
	
	private static Rank countSameRank(Card[] deck, int row) {
		int inRow = 0;
		Rank lastRank = null;
		
		for(Card c : deck) {
			if (c.getRank() == lastRank) {
				inRow++;
				if (++inRow == row) {
					return lastRank;
				}
			} else {
				inRow = 0;
				lastRank = c.getRank();
			}
		}
		
		return null;

	}
	
	private static Rank hasThree(Card[] deck) {
		return countSameRank(deck, 3);
		
	}
	
	private static Rank hasFour(Card[] deck) {
		return countSameRank(deck, 4);
		
	}
	
	private static boolean hasFullHouse(Card[] deck) {
		return hasThree(deck) != null && countPairs(deck) >= 1;
		
	}
	
	private static Rank isStraight(Card[] deck) {
		// TODO FIX!!!
		int i = 1, cnt = 0;
		Rank firstRank = deck[0].getRank();
		Rank lastRank = firstRank;

		while (i < deck.length && cnt < 5) {
			if (firstRank.ordinal() != deck[i].getRank().ordinal() - i)
				return null;
			else {
				i++;
				cnt++;
			}
		}
		
		return firstRank;

	}
	
	private static Suit isFlush(Card[] deck) {
		// TODO FIX!!!
		int i = 1, cnt = 0;
		Suit lastSuit = deck[0].getSuit();

		while (i < deck.length && cnt < 5) {
			if (lastSuit != deck[i].getSuit())
				return null;
			else {
				i++;
				cnt++;
			}
		}
		
		return lastSuit;

	}

}
