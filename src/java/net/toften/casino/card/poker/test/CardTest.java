package net.toften.casino.card.poker.test;

import java.util.List;

import net.toften.casino.card.Card;

import junit.framework.TestCase;

public class CardTest extends TestCase {

	/*
	 * Test method for 'net.toften.casino.card.poker.Card.newDeck()'
	 */
	public void testNewDeck() {
		List<Card> deck = Card.newDeck();
		
		for (Card c : deck) {
			System.out.println(c);
		}
		
		assertEquals(52, deck.size());
	}
	
	public void testShuffle() {
		List<Card> deck = Card.newDeck();

		Card.shuffle(deck);
		
		for (Card c : deck) {
			System.out.println(c);
		}
		
	}

}
