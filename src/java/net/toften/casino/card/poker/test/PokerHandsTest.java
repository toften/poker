package net.toften.casino.card.poker.test;

import junit.framework.TestCase;
import net.toften.casino.card.Card;
import net.toften.casino.card.Hand;
import net.toften.casino.card.Card.Rank;
import net.toften.casino.card.Card.Suit;
import net.toften.casino.card.poker.PokerHandTypes;
import net.toften.casino.card.poker.texasholdem.Round;

public class PokerHandsTest extends TestCase {
	private Hand h1, h2;
	private Round r;
	
	protected void setUp() throws Exception {
		super.setUp();
		
	}

	/*
	 * Test method for 'net.toften.casino.card.poker.PokerHandTypes.check(Hand, Round)'
	 */
	public void testCheck() {
		Card[] deck = new Card[5];
		
		deck[0] = new Card(Rank.TWO, Suit.DIAMONDS);
		deck[1] = new Card(Rank.TEN, Suit.SPADES);
		deck[2] = new Card(Rank.TEN, Suit.CLUBS);
		deck[3] = new Card(Rank.TWO, Suit.CLUBS);
		deck[4] = new Card(Rank.TEN, Suit.HEARTS);
		
		System.out.println(PokerHandTypes.check(deck));
	}

	/*
	 * Test method for 'net.toften.casino.card.poker.PokerHandTypes.convert(Hand, Round)'
	 */
	public void testConvert() {
	}
	
	private void printCards(Card[] deck) {
		for (Card c : deck)
			System.out.println(c);
	}

}
