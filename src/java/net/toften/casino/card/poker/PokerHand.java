package net.toften.casino.card.poker;

import net.toften.casino.card.Card;
import net.toften.casino.card.Hand;
import net.toften.casino.card.poker.texasholdem.Round;

public class PokerHand extends Hand implements Comparable<PokerHand> {

	public PokerHand(Round t, Card[] cards, int minCards) {
		super(t, cards, minCards, 5);
	}

	public PokerHandTypes getPokerHand() {
		return PokerHandTypes.check(this, t);
	}

	public int compareTo(PokerHand o) {
		if (getPokerHand().ordinal() > o.getPokerHand().ordinal())
			return 1;
	
		if (getPokerHand().ordinal() < o.getPokerHand().ordinal())
			return -1;
	
		// The hands are the same
		switch (getPokerHand()) {
		case FLUSH:
		case ROYALFLUSH:
	
		}
		
		return 0;
	}

}
