package net.toften.casino.card;

import java.util.List;

public class Deck {
	private List<Card> deck;

	public Deck() {
		deck = Card.newDeck();
		
		Card.shuffle(deck);
	}
	
	public Card[] getCards(int number) {
		if (number > deck.size())
			throw new IllegalStateException("Not enouth cards in deck");
		
		Card[] r = new Card[number];
		
		for (int i = 0; i < r.length; i++) {
			r[i] = deck.get(0);
			deck.remove(0);
		}
		
		return r;
	}

	public void burnCards(int number) {
		if (number > deck.size())
			throw new IllegalStateException("Not enouth cards in deck");
		
		while (number-- > 0)
			deck.remove(0);
		
	}
}
