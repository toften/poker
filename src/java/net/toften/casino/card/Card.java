package net.toften.casino.card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Card implements Comparable<Card> {
	public enum Rank { TWO, THREE, FOUR, FIVE, SIX, SEVEN,
		EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
	}
	
	public enum Suit { CLUBS, DIAMONDS, HEARTS, SPADES }
	
	private final Rank rank;
	private final Suit suit;
	
	private static final List<Card> protoDeck = new ArrayList<Card>();
	
	static {
		for (Suit suit : Suit.values())
			for (Rank rank : Rank.values())
				protoDeck.add(new Card(rank, suit));
	}
	
	/**
	 * @param rank
	 * @param suit
	 */
	public Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}

	public Rank getRank() {
		return rank;
	}

	public Suit getSuit() {
		return suit;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return rank + " of " + suit;
	}
	
	public static List<Card> newDeck() {
		return new ArrayList<Card>(protoDeck);
	}
	
	public static void shuffle(List<Card> deck) {
		Collections.shuffle(deck);
	}

	/**
	 * @param o
	 * @return
	 */
	public int compareTo(Card o) {
		int returnValue = 0;
		
		if (!this.equals(o)) {
			if (rank.ordinal() > o.getRank().ordinal())
				returnValue = 1;
			else if (rank.ordinal() < o.getRank().ordinal())
				returnValue = -1;
			else { // The rank is the same
				if (suit.ordinal() > o.getSuit().ordinal())
					returnValue = 1;
				else if (suit.ordinal() < o.getSuit().ordinal())
					returnValue = -1;
			}
		}
		
		return returnValue;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		
		if (!(obj instanceof Card))
			return false;
		
		Card o = (Card)obj;
		
		if (this == o)
			return true;
		
		if (rank == o.getRank() && suit == o.getSuit())
			return true;

		return false;
	}

	@Override
	public int hashCode() {
		return rank.ordinal() + 13 * suit.ordinal();
	}
	
}
