package net.toften.casino;

public interface Table {
	public int getPlayerCount();
	
	public PlayerInfo[] getPlayerInfo();
	
	/**
	 * A player can join the table using this method
	 * 
	 * @param p
	 * @return
	 */
	public Seat join(Membership p, double pot);
	
	public int getNumberOfSeats();
}
