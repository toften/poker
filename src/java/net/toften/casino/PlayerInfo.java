package net.toften.casino;

public interface PlayerInfo {
	public double getPot();
	
	public PlayerStatus getStatus();
}
