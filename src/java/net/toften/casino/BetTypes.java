package net.toften.casino;

public enum BetTypes {
	CHECK,
	RAISE,
	CALL,
	FOLD
}
