package net.toften.casino;

import java.util.List;

public interface Membership {

	public Account getAccount();

	public <T extends Table> List<T> getTables();

}
